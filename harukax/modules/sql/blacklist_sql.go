/*
 *    Haruka X (A telegram bot project)
 *    Copyright (C) 2020 Haruka Network Development
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package sql

import (
	"encoding/json"
	"fmt"

	"github.com/wI2L/jettison"

	"gitlab.com/HarukaNetwork/OSS/HarukaX/harukax/modules/utils/caching"
)

type BlackListFilters struct {
	ChatID  string `gorm:"primary_key" json:"chat_id"`
	Trigger string `gorm:"primary_key" json:"trigger"`
}

func AddToBlacklist(chatID string, trigger string) {
	filter := &BlackListFilters{ChatID: chatID, Trigger: trigger}
	SESSION.Save(filter)
	cacheBlacklist(chatID)
}

func RmFromBlacklist(chatID string, trigger string) bool {
	filter := &BlackListFilters{ChatID: chatID, Trigger: trigger}
	if SESSION.Delete(filter).RowsAffected == 0 {
		return false
	}
	cacheBlacklist(chatID)
	return true
}

func GetChatBlacklist(chatID string) []BlackListFilters {
	blf, err := caching.CACHE.Get(fmt.Sprintf("blacklist_%v", chatID))
	var blistFilters []BlackListFilters = nil
	if err != nil {
		blistFilters = cacheBlacklist(chatID)
	}

	_ = json.Unmarshal(blf, &blistFilters)
	return blistFilters
}

func cacheBlacklist(chatID string) []BlackListFilters {
	var filters []BlackListFilters
	SESSION.Where("chat_id = ?", chatID).Find(&filters)
	blJson, _ := jettison.Marshal(filters)
	_ = caching.CACHE.Set(fmt.Sprintf("blacklist_%v", chatID), blJson)
	return filters
}
