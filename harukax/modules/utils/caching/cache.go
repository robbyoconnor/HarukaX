/*
 *    Haruka X (A telegram bot project)
 *    Copyright (C) 2020 Haruka Network Development
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package caching

import (
	"time"

	"gitlab.com/HarukaNetwork/OSS/HarukaX/harukax/modules/utils/error_handling"
	"github.com/allegro/bigcache"
)

var CACHE *bigcache.BigCache

func InitCache() {
	config := bigcache.Config{
		Shards:             1024,
		LifeWindow:         30 * 24 * time.Hour, // one month
		CleanWindow:        5 * time.Minute,
		MaxEntriesInWindow: 1000 * 10 * 60,
		MaxEntrySize:       500,
		HardMaxCacheSize:   512,
		OnRemove:           nil,
		OnRemoveWithReason: nil,
	}
	cache, err := bigcache.NewBigCache(config)
	error_handling.HandleErr(err)
	CACHE = cache
}
