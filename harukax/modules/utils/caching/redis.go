/*
 *    Haruka X (A telegram bot project)
 *    Copyright (C) 2020 Haruka Network Development
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package caching

import (
	"time"

	"gitlab.com/HarukaNetwork/OSS/HarukaX/harukax"
	"github.com/go-redis/redis"
)

var REDIS *redis.Client

func InitRedis() {
	client := redis.NewClient(&redis.Options{
		Addr:         harukax.BotConfig.RedisAddress,
		Password:     harukax.BotConfig.RedisPassword,
		DB:           0,
		DialTimeout:  time.Second,
		MinIdleConns: 0,
	})
	REDIS = client
}
